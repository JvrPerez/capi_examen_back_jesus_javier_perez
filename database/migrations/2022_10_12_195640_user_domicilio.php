<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_domicilio', function(Blueprint $table){
            $table->bigInteger('user_id')->unsigned();
            $table->string('domicilio',200);
            $table->string('num_exterior',6);
            $table->string('colonia',50);
            $table->string('cp',5);
            $table->string('ciudad',50);
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_domicilio');
    }
};
